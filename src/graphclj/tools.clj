(ns graphclj.tools
  (:require [clojure.string :as str]
            [graphclj.graph :as graph]
            [graphclj.centrality :as central]))


(defn readfile [f]
    "Returns a sequence from a file f"
    (with-open [rdr (clojure.java.io/reader f)]
            (doall (line-seq rdr))))

(defn make_order [g]
  (let [k (sort (keys g)),order 0]
    (loop [k k,order order,res g]
      (if (seq k)
      (recur (rest k) (+ (get res (first k)) (+ 1 order)) (assoc res (first k) order))
      res))))

(defn make_list [g l]
  (loop [v (keys g),res {}]
    (if (seq v)
      (if (contains? res (get (get g (first v)) l))
        (recur (rest v) (assoc res (get (get g (first v)) l) (+ 1 (get res (get (get g (first v)) l)))))
        (recur (rest v) (assoc res (get (get g (first v)) l) 0)))
      (make_order res))))

(defn rank-nodes [g,l]
  "Ranks the nodes of the graph in relation to label l in accending order"
  (loop [v g,res g]
    (if (seq v)
      (recur (rest v) (assoc res (first (first v)) (assoc (get res (first (first v))) :rank (get (make_list g l) (get (get res (first (first v))) l)))))
      res))
)

(defn generate-colors [n]
    (let [step 10]
    (loop [colors {}, current [255.0 160.0 122.0], c 0]
      (if (= c (inc n))
        colors
        (recur (assoc colors c (map #(/ (mod (+ step %) 255) 255) current))
               (map #(mod (+ step %) 255) current) (inc c))
      ))))

(defn color-add [c n]
  (let [v (get c n)]
    (str " [style=filled color=\""
      (nth v 0) " "
      (nth v 1) " "
      (nth v 2)
      "\"]\n")))

(defn find-lowest-rank [g]
  (loop [g g,max 0]
    (if (seq g)
      (if (> (get (second (first g)) :rank) max)
        (recur (rest g) (get (second (first g)) :rank))
        (recur (rest g) max))
      max)))

(defn pair-check [n p_1 p_2]
  (if (contains? n #{p_1 p_2})
    true
    false))

(defn add-line [n s r ans]
  (loop [n n,s (seq s),r r,ans ans]
    (if (seq s)
      (if (pair-check r n (first s))
        (recur n (rest s) r ans)
        (recur n (rest s) (conj r #{n (first s)}) (str ans n " -- " (first s) "\n")))
      (vector r ans))))

(defn line-check [g]
  (loop [k (sort (keys g)),s #{},res ""]
    (if (seq k)
      (let [x_1 (first (add-line (first k) (get (get g (first k)) :neigh) s res))
        x_2 (second (add-line (first k) (get (get g (first k)) :neigh) s res))
        ](recur (rest k) x_1 x_2))
      res)))

(defn to-dot [g]
  "Returns a string in dot format for graph g, each node is colored in relation to its ranking"
  (let [res "graph g{\n",colors (generate-colors (find-lowest-rank g))]
    (loop [g g,k (sort (keys g)),res res]
      (if (seq k)
        (recur g (rest k) (str res (first k) (color-add colors (get (get g (first k)) :rank))))
        (str res (line-check g) "}"))))
)
