(ns graphclj.core
  (:require [clojure.string :refer [trim]]
            [graphclj.graph :as graph]
            [graphclj.tools :as tools]
            [graphclj.centrality :as central])
  (:gen-class))

(defn -main
  [& args]
  (do
    (println "--- Functions list ---")
    (println "1. gen-graph")
    (println "2. erdos-renyi-rnd")
    (println "3. degrees")
    (println "4. distance")
    (println "5. closeness")
    (println "6. closeness-all")
    (println "7. rank-node")
    (println "8. to-dot")
    (println "9. QUIT")

    (println "\nGraph g = {1 {:neigh #{0 4 3}},0 {:neigh #{1 3}},3 {:neigh #{0 1 2}},4 {:neigh #{1}},2 {:neigh #{3}}}")
    (loop []
      (let [g {1 {:neigh #{0 4 3}},0 {:neigh #{1 3}},3 {:neigh #{0 1 2}},4 {:neigh #{1}},2 {:neigh #{3}}}]
        (print "\nChoose the number of function : ")
        (println "ex) 3") (flush) (def func (read-line))
        (if (= func "1")
          (do
            (println "gen-graph [\"0 1\" \"2 3\" \"0 2\"]")
            (println "Result -> ",(graph/gen-graph ["0 1" "2 3" "0 2"]))
            (recur))
          (if (= func "2")
            (do
              (println "erdos-renyi-rnd 5 0.5")
              (println "Result -> ",(graph/erdos-renyi-rnd 5 0.5))
              (recur))
            (if (= func "3")
              (do
                (println "Result -> ",(central/degrees g))
                (recur))
              (if (= func "4")
                (do
                  (println "Result -> ",(central/distance g 0))
                  (recur))
                (if (= func "5")
                  (do
                    (println "Result -> ",(central/closeness g 0))
                    (recur))
                  (if (= func "6")
                    (do
                      (println "Result -> ",(central/closeness-all g))
                      (recur))
                    (if (= func "7")
                      (do
                        (println "Result -> ",(tools/rank-nodes (central/closeness-all g) :close))
                        (recur))
                      (if (= func "8")
                        (do
                          (println "Result -> ",(tools/to-dot (tools/rank-nodes (central/closeness-all g) :close)))
                          (recur))
                        (if (= func "9")
                          (System/exit 0)
                          (do
                            (println "Wrong number...")
                            (recur))))))))))))
  ))
)
