(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [clojure.set :as set]))
(require '[clojure.set :refer [difference]])



(defn degrees [g]
  "Calculates the degree centrality for each node"
  (let [key (keys g)]
    (loop [key key,res g]
      (if (seq key)
        (recur (rest key) (assoc res (first key) (assoc (get res (first key)) :degree (count (get (get g (first key)) :neigh)))))
        res)))
)

(defn init_distance [g]
  (loop [k (keys g), res {}]
    (if (seq k)
      (recur (rest k) (assoc res (first k) [0.0 "opened"]))
      res)))

(defn increase_distance [g src_dis dest_dis]
  (let [x g
    x (assoc x dest_dis [(inc (get (get x src_dis) 0)) (get (get x dest_dis) 1)])]
    x))

(defn status_change [g n]
  (let [x g
    x (assoc x n [(get (get x n) 0) "closed"])]
    x))

(defn make_form [g]
  (loop [k (keys g),res {}]
    (if (seq k)
      (recur (rest k) (assoc res (first k) (get (get g (first k)) 0)))
      res)))

(defn distance [g n]
  "Calculate the distances of one node to all the others"
  (let [res (init_distance g)
    res (status_change res n)]
    (loop [g g,n n,searched [],sets (get (get g n) :neigh),res res]
      (if (empty? g)
        (make_form res)
        (if (seq sets)
          (if (= (get (get res (first sets)) 1) "opened")
            (recur g n (conj searched (first sets)) (rest sets) (increase_distance (status_change res (first sets)) n (first sets)))
            (recur g n searched (rest sets) res))
            (recur (dissoc g n) (first searched) (rest searched) (get (get g (first searched)) :neigh) res)))))
)

(defn inverse [n]
  (if (== n 0)
    0.0
    (/ 1.0 n)))

(defn closeness [g n]
  "Returns the closeness for node n in graph g"
  (let [dis (distance g n)]
    (loop [dis_val (vals dis),res 0]
      (if (seq dis_val)
        (recur (rest dis_val) (+ res (inverse (first dis_val))))
        res)))
)


(defn closeness-all [g]
  "Returns the closeness for all nodes in graph g"
  (let [key (keys g)]
    (loop [key key,res g]
      (if (seq key)
        (recur (rest key) (assoc res (first key) (assoc (get res (first key)) :close (closeness g (first key)))))
        res)))
)
