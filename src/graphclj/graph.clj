(ns graphclj.graph
  (:require [clojure.string :as str]))
(use '[clojure.string :only (split triml)])

(defn form_change [str]
  (loop [s str, res []]
    (if (seq s)
      (recur (pop s) (conj (conj res (get (split (peek s) #"\s+") 1)) (get (split (peek s) #"\s+") 0)))
      res)))

;; Generate a graph from the lines
(defn gen-graph [lines]
    "Returns a hashmap contating the graph"
      (loop [v lines,res {}]
        (if (seq v)
          (if (contains? res (get (split (peek v) #"\s+") 0))
            (if (contains? res (get (split (peek v) #"\s+") 1))
              (let [temp res
                temp (assoc temp (get (split (peek v) #"\s+") 0) (assoc {} :neigh (conj (get (get res (get (split (peek v) #"\s+") 0)) :neigh) (get (split (peek v) #"\s+") 1))))
                temp (assoc temp (get (split (peek v) #"\s+") 1) (assoc {} :neigh (conj (get (get res (get (split (peek v) #"\s+") 1)) :neigh) (get (split (peek v) #"\s+") 0))))
                ]
                (recur (pop v) temp))
              (let [temp res
                temp (assoc temp (get (split (peek v) #"\s+") 0) (assoc {} :neigh (conj (get (get res (get (split (peek v) #"\s+") 0)) :neigh) (get (split (peek v) #"\s+") 1))))
                temp (assoc temp (get (split (peek v) #"\s+") 1) (assoc {} :neigh (set (get (split (peek v) #"\s+") 0))))
                ]
                (recur (pop v) temp)))
            (if (contains? res (get (split (peek v) #"\s+") 1))
              (let [temp res
                temp (assoc temp (get (split (peek v) #"\s+") 0) (assoc {} :neigh (set (get (split (peek v) #"\s+") 1))))
                temp (assoc temp (get (split (peek v) #"\s+") 1) (assoc {} :neigh (conj (get (get res (get (split (peek v) #"\s+") 1)) :neigh) (get (split (peek v) #"\s+") 0))))
                ]
                (recur (pop v) temp))
              (let [temp res
                temp (assoc temp (get (split (peek v) #"\s+") 0) (assoc {} :neigh (set (get (split (peek v) #"\s+") 1))))
                temp (assoc temp (get (split (peek v) #"\s+") 1) (assoc {} :neigh (set (get (split (peek v) #"\s+") 0))))
                ]
                (recur (pop v) temp))))
              res))
)

(defn erdos-renyi-rnd [n,p]
  "Returns a G_{n,p} random graph, also known as an Erdős-Rényi graph"

  (loop [n n,p p,a 0,b (dec n),res [],init n]
    (if (> n 0)
      (if (> b a)
        (if (< (rand) p)
          (recur n p a (dec b) (conj res (str a " " b)) init)
          (recur n p a (dec b) res init))
        (recur (dec n) p (inc a) (dec init) res init))
      (gen-graph res)))
)
